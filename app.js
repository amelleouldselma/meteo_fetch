const weathersIcons = {
    "Rain": "wi wi-day-rain",
    "Clouds" : "wi wi-day-cloudy",
    "Clear" : "wi wi-day-sunny",
    "Snow" : "wi wi-day-snow",
    "mist" : "wi wi-day-fog",
    "Drizzle" : "wi wi-day-sleet",
}

function capitalize(str){
    return str[0].toUpperCase() + str.slice(1);
}


//Finction qui permet de rcécuperer l'adresse Ip du pc 
async function main(){

    const ip = await fetch('https://api.ipify.org?fromat=json') 

        .then(resultat => resultat.json())
        .then(json => json.ip);

    const ville = await fetch('http://freegeoip.net/json/' + ip)
        .then(resultat => resultat.json())
        .then(json => json.city);

    const meteo = await fetch('http://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=eb1b8e60fc0619db83ee9a51573b3a0d&lang=fr&units=metric')
            .then(resultat => resultat.json())
            .then(json => json)


    console.log(meteo);
    
    displayWeatherInfos(meteo)

}

function displayWeatherInfos(data){
    const name = data.name;
    const temperature = data.main.temp;
    const conditions = data.weather[0].main;
    const description = data.weather[0].description;

    document.querySelector('#ville').textContent = name;
    document.querySelector('#temperature').textContent = Math.round(temperature);
    document.querySelector('#conditions').textContent = capitalize(description);
    
    document.querySelector('i.wi').className = weatherIcons[conditions];

    document.body.className = conditions.toLowerCase();
}

main();